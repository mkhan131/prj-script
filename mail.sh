#!/bin/bash -   

#Title          :mail.sh
#Description    :Configure the mail server
#Author         :Muhammad Khan
#Date           :20170611
#Notes          :NA     
#Usage          :./mail.sh
#Version        :1.0    

#============================================================================        
echo "Configuring mail server..."
./postfix_setup.sh
./dovecot_setup.sh





