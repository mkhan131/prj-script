#!/bin/bash -   

#Title          :postfix_setup.sh
#Description    :NA     
#Author         :Muhammad Khan
#Date           :20170609
#Notes          :NA     
#Usage          :./postfix_setup.sh
#Version        :2.0    

#============================================================================        
timedatectl set-ntp yes
yum install telnet -y


#sudo yum  install postfix -y
sudo systemctl start postfix

echo " configuring postfix....."

cat > /etc/postfix/main.cf <<EOF
 
queue_directory = /var/spool/postfix

command_directory = /usr/sbin

daemon_directory = /usr/libexec/postfix

data_directory = /var/lib/postfix

mail_owner = postfix

inet_interfaces = all

#inet_interfaces = $myhostname

#inet_interfaces = $myhostname, localhost

#inet_interfaces = localhost

# Enable IPv4, and IPv6 if supported

inet_protocols = all

mydestination = $myhostname, localhost.$mydomain, localhost, $mydomain

mydestination = $myhostname, localhost.$mydomain, localhost, $mydomain

mydestination = $myhostname, localhost.$mydomain, localhost, $mydomain,

#    mail.$mydomain, www.$mydomain, ftp.$mydomain

unknown_local_recipient_reject_code = 550

alias_database = hash:/etc/aliases

#home_mailbox = Mailbox

home_mailbox = Maildir/

#

#mail_spool_directory = /var/mail

#mail_spool_directory = /var/spool/mail

debug_peer_level = 2

debugger_command =

     PATH=/bin:/usr/bin:/usr/local/bin:/usr/X11R6/bin

     ddd $daemon_directory/$process_name $process_id & sleep 5

 

sendmail_path = /usr/sbin/sendmail.postfix

newaliases_path = /usr/bin/newaliases.postfix

mailq_path = /usr/bin/mailq.postfix

setgid_group = postdrop

html_directory = no

# manpage_directory: The location of the Postfix on-line manual pages.

#

manpage_directory = /usr/share/man

# sample_directory: The location of the Postfix sample configuration files.

# This parameter is obsolete as of Postfix 2.1.

#

sample_directory = /usr/share/doc/postfix-2.10.1/samples

# readme_directory: The location of the Postfix README files.

#

readme_directory = /usr/share/doc/postfix-2.10.1/README_FILE

# CODE ADDED

smtpd_sasl_type = dovecot


# Can be an absolute path, or relative to $queue_directory

# Debian/Ubuntu users: Postfix is setup by default to run chrooted, so it is best to leave it as-is below

smtpd_sasl_path = private/auth


# On Debian Wheezy path must be relative and queue_directory defined

#queue_directory = /var/spool/postfix


# and the common settings to enable SASL:

smtpd_sasl_auth_enable = yes

# With Postfix version before 2.10, use smtpd_recipient_restrictions

smtpd_relay_restrictions = permit_mynetworks, permit_sasl_authenticated, reject_unauth_destination

EOF
echo " Postfix configured Successfully"

systemctl restart postfix



