#!/bin/bash -   

#Title          :main.sh
#Description    :The main script for the project
#Author         :Muhammad Khan
#Date           :20170611
#Notes          :The script will execute other scripts necessary to configure the VMs as per project requirement.
#Usage          :./main.sh
#Version        :1.0    

#============================================================================        
echo "Welcome to NASP19 - S09 Scripting Projecth"

./ospfd.sh
./dhcpd.sh
./iptables_setup.sh
./nsd_setup.sh
./unbound_setup.sh
./hostapd.sh

echo "Router is configured successfully" 
exit





