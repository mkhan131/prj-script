#!/bin/bash -   

#Title          :setup. sh
#Description    :Preparing system for a fully automated project completion
#Author         :Muhammad Khan
#Date           :20170604
#Notes          :This script will fullfil pre-requiste on any system by installing Vagrant, Virtualbox for automation
#Usage          :./setup.sh
#Version        :1.0    

#============================================================================        
#creating vagrant box with packer
./packer build centos.json
vagrant box add -f router centos7.box
vagrant box add -f mail centos7.box
vagrant up


