#!/bin/bash -   

#Title          :dhcpd.sh
#Description    :DHCP Configuration
#Author         :Muhammad Khan
#Date           :20170607
#Notes          :This wil configure the DHCP on router
#Usage          :./dhcpd.sh
#Version        :1.0    

#============================================================================        
set -o nounset
echo "step-1"

cat > /etc/systemd/system/dhcpd.service <<EOF 
[Unit]
Description=DHCPv4 Server Daemon
Documentation=man:dhcpd(8) man:dhcpd.conf(5)
Wants=network-online.target
After=network-online.target
After=time-sync.target

[Service]
Type=notify
ExecStart=/usr/sbin/dhcpd -f -cf /etc/dhcp/dhcpd.conf -user dhcpd -group dhcpd --no-pid wlp0s11u1 eth1

[Install]
WantedBy=multi-user.target

EOF
# IMPORTANT - reload deamon to bring interface changes into effect
sudo systemctl daemon-reload

echo "installing DHCPD...."
sudo yum install dhcp -y

echo "Configuring DHCP service"

cat > /etc/dhcp/dhcpd.conf <<-EOF  

option  domain-name "s09.as.learn";

option  domain-name-servers 10.16.9.126;

#Network: 10.16.9.0/25

subnet 10.16.9.0 netmask 255.255.255.128 
	{

	  option routers 10.16.9.126;

	  range 10.16.9.100 10.16.9.125;

   #option domain-name-servers 10.16.9.126;

         }

subnet 10.16.9.128 netmask 255.255.255.128
	 {

  	 option routers 10.16.9.254; #routers option defines the default gateway for clients

         range 10.16.9.200 10.16.9.225;

        }

host mail {

            hardware ethernet 08:00:27:44:6F:20;
            fixed-address 10.16.9.1;

                   }

EOF
systemctl restart dhcpd
systemctl disable dhcpd
systemctl enable dhcpd



