#!/bin/bash -

echo "#+++++++++++++++++++++"
echo "Guest Pre-Requiste"
echo "#+++++++++++++++++++++"
sudo yum -y install gcc make gcc-c++ kernel-devel-`uname -r` perl

echo "#==============================================================================="
echo "Creating mount point, mounting, and installing VirtualBox Guest Additions"
echo "#==============================================================================="


#VBOX_VERSION=$(cat /home/vagrant/.vbox_version)

#cd /tmp
mkdir /mnt
#mount -o loop /home/vagrant/VBoxGuestAdditions_$VBOX_VERSION.iso /mnt
mount -o loop /home/vagrant/VBoxGuestAdditions.iso /mnt


sh /mnt/VBoxLinuxAdditions.run

umount /mnt

rm -rf /home/vagrant/VBoxGuestAdditions_*.iso
