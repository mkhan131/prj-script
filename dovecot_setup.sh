#!/bin/bash -   

#Title          :dovecot_setup.sh
#Description    :Retreiving Mail Setup
#Author         :Muhammad Khan
#Date           :20170609
#Notes          :Open surce mail setup
#Usage          :./dovecot_setup.sh
#Version        :1.0    

#============================================================================        

echo "Installing dovecot"
sudo yum install dovecot -y
echo " Configuring dovecot...."
cat > /etc/dovecot/dovecot.cf <<-EOF
## Dovecot configuration file

# If you're in a hurry, see http://wiki2.dovecot.org/QuickConfiguration

# "doveconf -n" command gives a clean output of the changed settings. Use it

# instead of copy&pasting files when posting to the Dovecot mailing list.
# '#' character and everything after it is treated as comments. Extra spaces

# and tabs are ignored. If you want to use either of these explicitly, put the

# value inside quotes, eg.: key = "# char and trailing whitespace  "
# Most (but not all) settings can be overridden by different protocols and/or

# source/destination IPs by placing the settings inside sections, for example:

# protocol imap { }, local 127.0.0.1 { }, remote 10.0.0.0/8 { }

# Default values are shown for each setting, it's not required to uncomment

# those. These are exceptions to this though: No sections (e.g. namespace {})

# or plugin settings are added by default, they're listed only as examples.

# Paths are also just examples with the real defaults being based on configure

# options. The paths listed here are for configure --prefix=/usr

# --sysconfdir=/etc --localstatedir=/var

# Protocols we want to be serving.

protocols = imap pop3 lmtp


# A comma separated list of IPs or hosts where to listen in for connections.

# "*" listens in all IPv4 interfaces, "::" listens in all IPv6 interfaces.

# If you want to specify non-default ports or anything more complex,

# edit conf.d/master.conf.

#listen = *, ::

# Base directory where to store runtime data.

#base_dir = /var/run/dovecot/

# Name of this instance. In multi-instance setup doveadm and other commands

# can use -i <instance_name> to select which instance is used (an alternative

# to -c <config_path>). The instance name is also added to Dovecot processes

# in ps output.

#instance_name = dovecot

# Greeting message for clients.

#login_greeting = Dovecot ready.

# Space separated list of trusted network ranges. Connections from these

# IPs are allowed to override their IP addresses and ports (for logging and

# for authentication checks). disable_plaintext_auth is also ignored for

# these networks. Typically you'd specify your IMAP proxy servers here.

#login_trusted_networks =

# Space separated list of login access check sockets (e.g. tcpwrap)

#login_access_sockets =

# With proxy_maybe=yes if proxy destination matches any of these IPs, don't do

# proxying. This isn't necessary normally, but may be useful if the destination

# IP is e.g. a load balancer's IP.

#auth_proxy_self =

# Show more verbose process titles (in ps). Currently shows user name and

# IP address. Useful for seeing who are actually using the IMAP processes

# (eg. shared mailboxes or if same uid is used for multiple accounts).

#verbose_proctitle = no

# Should all processes be killed when Dovecot master process shuts down.

# Setting this to "no" means that Dovecot can be upgraded without

# forcing existing client connections to close (although that could also be

# a problem if the upgrade is e.g. because of a security fix).

#shutdown_clients = yes

# If non-zero, run mail commands via this many connections to doveadm server,

# instead of running them directly in the same process.

#doveadm_worker_count = 0

# UNIX socket or host:port used for connecting to doveadm server

#doveadm_socket_path = doveadm-server

# Space separated list of environment variables that are preserved on Dovecot

# startup and passed down to all of its child processes. You can also give

# key=value pairs to always set specific settings.

#import_environment = TZ
##

## Dictionary server settings

##

# Dictionary can be used to store key=value lists. This is used by several

# plugins. The dictionary can be accessed either directly or though a

# dictionary server. The following dict block maps dictionary names to URIs

# when the server is used. These can then be referenced using URIs in format

# "proxy::<name>".
dict {

  #quota = mysql:/etc/dovecot/dovecot-dict-sql.conf.ext

  #expire = sqlite:/etc/dovecot/dovecot-dict-sql.conf.ext

}

# Most of the actual configuration gets included below. The filenames are

# first sorted by their ASCII value and parsed in that order. The 00-prefixes

# in filenames are intended to make it easier to understand the ordering.

!include conf.d/*.conf

# A config file can also tried to be included without giving an error if

# it's not found:

!include_try local.conf

EOF
# mail location
echo "

#default_process_limit = 100

#default_client_limit = 1000

# Default VSZ (virtual memory size) limit for service processes. This is mainly

# intended to catch and kill processes that leak memory before they eat up

# everything.

#default_vsz_limit = 256M

# Login user is internally used by login processes. This is the most untrusted

# user in Dovecot system. It shouldn't have access to anything at all.

#default_login_user = dovenull
# Internal user is used by unprivileged processes. It should be separate from

# login user, so that login processes can't disturb other processes.

#default_internal_user = dovecot
service imap-login {

  inet_listener imap {

	#port = 143

  }

  inet_listener imaps {

	#port = 993

	#ssl = yes

  }

  # Number of connections to handle before starting a new process. Typically

  # the only useful values are 0 (unlimited) or 1. 1 is more secure, but 0

  # is faster. <doc/wiki/LoginProcess.txt>

  #service_count = 1

  # Number of processes to always keep waiting for more connections.

  #process_min_avail = 0

  # If you set service_count=0, you probably need to grow this.

  #vsz_limit = $default_vsz_limit

}

service pop3-login {

  inet_listener pop3 {

	#port = 110

  }

  inet_listener pop3s {

	#port = 995

	#ssl = yes

  }

}

 

service lmtp {

  unix_listener lmtp {

	#mode = 0666

  }

  # Create inet listener only if you can't use the above UNIX socket

  #inet_listener lmtp {

	# Avoid making LMTP visible for the entire internet

	#address =

	#port =

  #}

}

 

service imap {

  # Most of the memory goes to mmap()ing files. You may need to increase this

  # limit if you have huge mailboxes.

  #vsz_limit = $default_vsz_limit

  # Max. number of IMAP processes (connections)

  #process_limit = 1024

}

 

service pop3 {

  # Max. number of POP3 processes (connections)

  #process_limit = 1024

}

service auth {

  # auth_socket_path points to this userdb socket by default. It's typically

  # used by dovecot-lda, doveadm, possibly imap process, etc. Users that have

  # full permissions to this socket are able to get a list of all usernames and

  # get the results of everyone's userdb lookups.

  #

  # The default 0666 mode allows anyone to connect to the socket, but the

  # userdb lookups will succeed only if the userdb returns an "uid" field that

  # matches the caller process's UID. Also if caller's uid or gid matches the

  # socket's uid or gid the lookup succeeds. Anything else causes a failure.

  #

  # To give the caller full permissions to lookup all users, set the mode to

  # something else than 0666 and Dovecot lets the kernel enforce the

  # permissions (e.g. 0777 allows everyone full permissions).

  #unix_listener auth-userdb {

unix_listener /var/spool/postfix/private/auth {

	mode = 0666

  # Assuming the default Postfix user and group

	user = postfix

	group = postfix   

  }

  # Postfix smtp-auth

  #unix_listener /var/spool/postfix/private/auth {

  #  mode = 0666

  #}

 # Auth process is run as this user.

  #user = $default_internal_user

}

service auth-worker {

  # Auth worker process is run as root by default, so that it can access

  # /etc/shadow. If this isn't necessary, the user should be changed to

  # $default_internal_user.

  #user = root

}

service dict {

  # If dict proxy is used, mail processes should have access to its socket.

  # For example: mode=0660, group=vmail and global mail_access_groups=vmail

  unix_listener dict {

	#mode = 0600

	#user =

	#group =

  }

}

" > /etc/dovecot/conf.d/10-auth.conf


sudo systemctl start dovecot
sudo systemctl enable dovecot
echo "Mail solution configured Successfully"
