# Scripting Project Verson 3.0#

## Prerequisite ##

1. Internet Access
1. Virtualbox
1. Vagrant

## Setup Instructions ##

Download "Project_Final_V3" tag and run "vagrant up"  


## Project Overview ##
Standalone Vagrant file with inline scripts which can be sent through email and will be able to fetch all relevant file from internet and bitbucket automatically.

### Known Issue ###

Please restart dhcpd on router and ifdown/ifup eth0 on mail VM as still working to resolve the issue of mail machine having trouble in ssh most probably due to 2 dhcp's on mail one for ensp03 and one for eth0.