#!/bin/bash -   

#Title          :nsd_setup.sh
#Description    :Configures nsd on the Router
#Author         :Muhammad Khan
#Date           :20170604
#Notes          :nsd recursive server to resolve internal names
#Usage          :./nsd_setup.sh
#Version        :1.0    

#============================================================================        
set -o nounset
echo "onfiguring nsd for internal DNS queries."
sudo yum install install nsd -y

# include 3 files : nsd.conf
#                 : s09.as.learn.zone
#                 : 9.16.10.in-addr.arpa.zone
echo "******************* Configuring nsd file ***********************"

cat > /etc/nsd/nsd.conf <<EOF
 
#nsd.conf -- the NSD(8) configuration file, nsd.conf(5).

# Copyright (c) 2001-2011, NLnet Labs. All rights reserved.



server:

    	# uncomment to specify specific interfaces to bind (default are the

    	# wildcard interfaces 0.0.0.0 and ::0).

    	# For servers with multiple IP addresses, list them one by one,

    	ip-address: 10.16.255.9

#ip-address: 127.0.0.1

    	# listen on IPv4 connections - yes is the default left commented

    	# do-ip4: yes

    	# listen on IPv6 connections

    	do-ip6: no

    	# port to answer queries on. default is 53 left commented

    	# port: 53

    	# After binding socket, drop user privileges.

    	# can be a username, id or id.gid.

    	# when looking for running processes nsd will be running as this user

    	# username: nsd


    	# The directory for zonefiles.

    	# Use default, left commented

    	#

    	#zonesdir: "/etc/nsd"

    	# Optional local server config

    	include: "/etc/nsd/server.d/*.conf"

    	# Include optional local configs.

    	include: "/etc/nsd/conf.d/*.conf"

# Remote control config section.

remote-control:

    	# Enable remote control with nsd-control(8) here.

    	# set up the keys and certificates with nsd-control-setup.

    	control-enable: yes


    	# what interfaces are listened to for control, default is on localhost.

    	# use default leave commented

    	#

    	# control-interface: 127.0.0.1

    	# control-interface: ::1

    	# port number for remote control operations (uses TLS over TCP).

    	# use default leave commented

    	#

    	# control-port: 8952

    	# nsd server key file for remote control.

    	# use default leave commented

    	#

    	# server-key-file: "/etc/nsd/nsd_server.key"

    	# nsd server certificate file for remote control.

    	# use default leave commented

    	#

    	# server-cert-file: "/etc/nsd/nsd_server.pem"

    	# nsd-control key file.

    	# use default leave commented

    	#

    	# control-key-file: "/etc/nsd/nsd_control.key"

    	# nsd-control certificate file.

    	# use default leave commented

    	#

    	# control-cert-file: "/etc/nsd/nsd_control.pem"

# Fixed zone entries (i.e. not added with nsd-control addzone)

zone:

    	name: "s09.as.learn"

    	zonefile: "s09.as.learn.zone"

zone:

    	name: "9.16.10.in-addr.arpa"

    	zonefile: "9.16.10.in-addr.arpa.zone"

EOF
 


echo "**************** Configuring Forward Look up zone file ********************"

cat > /etc/nsd/s09.as.learn.zone <<EOF
;zone file for s09.as.learn

$TTL 10s                          	; 10 secs default TTL for zone

s09.as.learn.   IN  SOA   rtr.s09.as.learn. htp.bcit.ca. (

                    	2014022500	; serial number of Zone Record

                    	1200s     	; refresh time

                    	180s      	; update retry time on failure

                    	1d        	; expiration time

                    	3600      	; cache time to live

                    	)

 

;Name servers for this domain

s09.as.learn.     	IN  	NS 	rtr.s09.as.learn.

 

;addresses of hosts

rtr.s09.as.learn. 	IN  	A   	10.16.9.126

mail.s09.as.learn.	IN  	A   	10.16.9.1

wirtr.s09.as.learn	IN  	A      	10.16.9.254    

s09.as.learn.     	IN  	MX  10  mail.s09.as.learn.

EOF
 
 echo "********************* Configuring Reverse Look up zone file **************"

cat > /etc/nsd/9.16.10.in-addr.arpa.zone <<EOF
;zone file for 10.16.255.9 / s09.as.learn reverse lookup

$TTL 10s; 10 secs default TTL for zone

9.16.10.in-addr.arpa.     	IN  	SOA 	rtr.s09.as.learn. htp.bcit.ca. (

                    	2016030011	; serial number of Zone Record

                    	1200s     	; refresh time

                    	180s      	; retry time on failure

                    	1d        	; expiration time

                    	3600      	; cache time to live

                    	)
;Name servers for this domain

9.16.10.in-addr.arpa.     	IN  	NS 	s09.as.learn.

;IP to Hostname Pointers

126.9.16.10.in-addr.arpa.   	IN  	PTR  	rtr.s09.as.learn.

1.9.16.10.in-addr.arpa.     	IN  	PTR  	mail.s09.as.learn.

254.9.16.10.in-addr.arpa.    IN    PTR     wirtr.s09.as.learn.

EOF

systemctl disable nsd
systemctl enable nsd

systemctl start nsd


echo "nsd is configured Successfully"





