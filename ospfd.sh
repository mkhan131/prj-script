#!/bin/bash -   

#Title          :ospfd.sh
#Description    :Configures OSPF routing Protocol
#Author         :Muhammad Khan
#Date           :20170607
#Notes          :NA     
#Usage          :./ospfd.sh
#Version        :1.0    

#============================================================================        
set -o nounset

echo " Installing Quagga...."
sudo yum install quagga -y

echo "Setting Up OSPF for the ROUTER"

#change ownership of quagga directory

chown quagga:quagga /etc/quagga/

systemctl enable zebra ospfd

systemctl start zebra ospfd

echo "Setting up ZEBRA configuration file"

cat > /etc/quagga/zebra.conf <<-EOF
!

! Zebra configuration saved from vty

!   2017/04/24 16:22:12

!

hostname rtr.s09.as.learn

log file /var/log/quagga/quagga.log

!

interface eth0

 description ExternalNetowrk

 ip address 10.16.255.9/24

 ipv6 nd suppress-ra

!

interface eth1

 description StudentNetwork

 ip address 10.16.9.126/25

 ipv6 nd suppress-ra

!

interface lo

!

!

!

line vty

!
EOF




# OSPF Configuration 
echo "Setting up OSPF Configuration File"

cat > /etc/quagga/ospfd.conf <<-EOF

!

! Zebra configuration saved from vty

!   2017/04/24 16:22:12

!


hostname ospfd

password zebra

log stdout

!

!

!

interface eth0 

!

interface eth1

!

interface lo

!

interface wlp0s11u1

!

router ospf

 ospf router-id 10.16.255.9

 network 10.16.9.0/25 area 0

 network 10.16.9.128/25 area 0

 network 10.16.255.0/24 area 0

!

line vty

!

EOF




systemctl restart zebra ospfd


